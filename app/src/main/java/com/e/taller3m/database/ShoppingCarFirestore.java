package com.e.taller3m.database;

import androidx.room.Dao;

import com.e.taller3m.model.ShoppingCart;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QuerySnapshot;

@Dao
public interface ShoppingCarFirestore {
    void insertProductos(ShoppingCart... cartProducts);
    Task<QuerySnapshot> getAll();
    void deleteProduct(ShoppingCart product);
}