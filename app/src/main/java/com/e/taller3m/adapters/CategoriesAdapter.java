package com.e.taller3m.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.taller3m.R;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    ArrayList<String> categoriesList;

    public interface onItemClickListener{
        void onClick(int pos);
    }

    CategoriesAdapter.onItemClickListener mListener;

    public void setOnItemClickListener(onItemClickListener mListener) {
        this.mListener = mListener;
    }

    public CategoriesAdapter(ArrayList<String> categoriesList) {
        this.categoriesList = categoriesList;
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories, parent, false);
        return new CategoriesViewHolder(mView,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesViewHolder holder, int position) {

        String category = categoriesList.get(position);
        holder.tvCategories.setText(category);

    }

    @Override
    public int getItemCount() {

        return (categoriesList != null) ? categoriesList.size() : 0;
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder {

        TextView tvCategories;

        public CategoriesViewHolder(@NonNull View itemView, final CategoriesAdapter.onItemClickListener listener) {
            super(itemView);

            tvCategories = itemView.findViewById(R.id.tvCategories);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onClick(position);
                        }
                    }
                }
            });
        }
    }
}
