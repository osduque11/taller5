package com.e.taller3m.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.e.taller3m.R;
import com.e.taller3m.database.Product_Repository;
import com.e.taller3m.database.ShoppingCart_Repository;
import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;
import com.e.taller3m.views.fragments.CartFragment;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ProductViewHolder> {

    private View mView;
    private Context mContext;
    ArrayList<ShoppingCart> listProduct;
    private Product_Repository productRepository;
    private ShoppingCart_Repository shoppingCarFirestore;

    public void setData(ArrayList<ShoppingCart> listProductCart) {
        this.listProduct = listProductCart;
        notifyDataSetChanged();
    }

    public interface onItemClickListener {
        void onClick(int pos);
    }

    CartAdapter.onItemClickListener mListener;

    public CartAdapter(ArrayList<ShoppingCart> listProduct) {
        this.listProduct = listProduct;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cartproducts, parent, false);
        mContext = mView.getContext();
        productRepository = new Product_Repository(mContext);
        shoppingCarFirestore = new ShoppingCart_Repository(mContext);
        return new ProductViewHolder(mView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        final ShoppingCart shoppingCart = listProduct.get(position);

        assert shoppingCart.getName() != null;
        holder.tvName.setText(shoppingCart.getName());
        holder.tvQuantity.setText("Cantidad: " + String.valueOf(shoppingCart.getQuantity()));

        if (shoppingCart.getIdImage() != 0) {
            holder.ivImageProduct.setImageDrawable(AppCompatResources.getDrawable(mContext, shoppingCart.getIdImage()));
        }

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);

                dialog.setTitle(R.string.delete_product);

                dialog.setMessage(R.string.delete_item_product);

                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        productRepository.getByCodigo(shoppingCart.getCode())
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {
                                            Product productUpdate = documentSnapshot.toObject(Product.class);
                                            if (productUpdate != null) {
                                                productUpdate.setStock(productUpdate.getStock() + shoppingCart.getQuantity());
                                                productRepository.updateStockByProductCode(productUpdate);
                                                shoppingCarFirestore.deleteProduct(shoppingCart);
                                                updateData();

                                            }

                                        }
                                    }
                                });

                    }
                });

                dialog.show();
            }
        });
    }

    public void updateData() {
        shoppingCarFirestore.getAll().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                listProduct.clear();
                for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {
                    ShoppingCart shoppingCart = doc.toObject(ShoppingCart.class);
                    listProduct.add(shoppingCart);
                }
                notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        return listProduct != null ? listProduct.size() : 0;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView tvCategory;
        ImageView ivImageProduct;
        TextView tvName;
        TextView tvQuantity;
        ImageButton btnDelete;

        public ProductViewHolder(@NonNull View itemView, final CartAdapter.onItemClickListener listener) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategoryProduct);
            ivImageProduct = itemView.findViewById(R.id.ivImageProduct);
            tvName = itemView.findViewById(R.id.tvNameProduct);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            btnDelete = itemView.findViewById(R.id.ibDelete);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onClick(position);
                        }
                    }
                }
            });
        }
    }
}
