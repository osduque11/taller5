package com.e.taller3m.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.e.taller3m.R;
import com.e.taller3m.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private View mView;
    private Context mContext;

    public interface onItemClickListener {
        void onClick(int pos);
    }

    ProductAdapter.onItemClickListener mListener;

    ArrayList<Product> listProduct;

    public ProductAdapter(ArrayList<Product> listProduct) {

        this.listProduct = listProduct;
    }

    public void setData(ArrayList<Product> listProduct) {
        this.listProduct = listProduct;
        notifyDataSetChanged();
    }

    public void setListener(onItemClickListener mListener) {

        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products, parent, false);
        mContext = mView.getContext();
        return new ProductViewHolder(mView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = listProduct.get(position);
        holder.tvCategory.setText(product.getCategory());
        assert product.getName() != null;
        holder.tvName.setText(product.getName());
        holder.tvStock.setText("Stock: "+String.valueOf(product.getStock()));

        if (product.getIdImage() != 0) {
            //holder.ivImageProduct.setImageDrawable(AppCompatResources.getDrawable(mContext, product.getIdImage()));

            Picasso.get().load(product.getIdImage()).into(holder.ivImageProduct);
        }

    }

    @Override
    public int getItemCount() {
        return listProduct != null ? listProduct.size() : 0;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView tvCategory;
        ImageView ivImageProduct;
        TextView tvName;
        TextView tvStock;

        public ProductViewHolder(@NonNull View itemView, final ProductAdapter.onItemClickListener listener) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategoryProduct);
            ivImageProduct = itemView.findViewById(R.id.ivImageProduct);
            tvName = itemView.findViewById(R.id.tvNameProduct);
            tvStock = itemView.findViewById(R.id.tvStock);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onClick(position);
                        }
                    }
                }
            });
        }
    }
}
