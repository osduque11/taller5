package com.e.taller3m.views.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.e.taller3m.R;
import com.e.taller3m.adapters.CategoriesAdapter;
import com.e.taller3m.views.activities.ProductsActivity;

import java.util.ArrayList;

public class CategoriesFragment extends Fragment {

    RecyclerView rvCategories;
    LinearLayoutManager llManager;
    CategoriesAdapter categoriesAdapter;

    ArrayList<String> listCategories;

    View mView;
    Context mContext;


    public CategoriesFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_categories, container, false);
        mContext = mView.getContext();
        initUI();


        return mView;
    }

    private void initUI() {

        listCategories = new ArrayList<>();
        listCategories.add("Smartphone");
        listCategories.add("Tenis");
        listCategories.add("Morrales");
        listCategories.add("Juguetes");
        listCategories.add("Portatiles");

        rvCategories = mView.findViewById(R.id.rvCategories);
        categoriesAdapter = new CategoriesAdapter(listCategories);

        categoriesAdapter.setOnItemClickListener(new CategoriesAdapter.onItemClickListener() {
            @Override
            public void onClick(int pos) {
                String categorySel = listCategories.get(pos);
                Intent intent = new Intent(mContext, ProductsActivity.class);
                intent.putExtra("category",categorySel);
                startActivity(intent);
            }
        });

        llManager = new LinearLayoutManager(mContext,RecyclerView.VERTICAL,false);
        rvCategories.setLayoutManager(llManager);
        rvCategories.setAdapter(categoriesAdapter);

    }

}
