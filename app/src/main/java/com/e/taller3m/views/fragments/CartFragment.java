package com.e.taller3m.views.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.e.taller3m.R;
import com.e.taller3m.adapters.CartAdapter;
import com.e.taller3m.database.Product_Repository;
import com.e.taller3m.database.ShoppingCarFirestore;
import com.e.taller3m.database.ShoppingCart_Repository;
import com.e.taller3m.model.Product;
import com.e.taller3m.model.ShoppingCart;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class CartFragment extends Fragment implements View.OnClickListener {

    private ArrayList<ShoppingCart> listProductCart;

    private Product_Repository productRepository;
    private ShoppingCarFirestore shoppingCarFirestore;

    private RecyclerView rvCartproducts;
    private LinearLayoutManager llManager;
    private CartAdapter cartAdapter;
    private Context mContext;
    private View mView;
    private Button btnClearData;
    private AlertDialog.Builder builder;

    public CartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_cart, container, false);
        mContext = mView.getContext();

        listProductCart = new ArrayList<>();

        shoppingCarFirestore = new ShoppingCart_Repository(mContext);
        productRepository = new Product_Repository(mContext);

        builder = new AlertDialog.Builder(mContext);
        initUI();
        getData();
        setHasOptionsMenu(true); //Mostrar el menú, botón guardar compra

        return mView;
    }

    private void initUI() {
        btnClearData = mView.findViewById(R.id.btnClearData);
        rvCartproducts = mView.findViewById(R.id.rvCartproducts);
        cartAdapter = new CartAdapter(listProductCart);
        llManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        rvCartproducts.setAdapter(cartAdapter);
        rvCartproducts.setLayoutManager(llManager);
        btnClearData.setOnClickListener(this);
    }

    private void getData() {
        shoppingCarFirestore.getAll().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() { //Sincrono
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                listProductCart.clear();
                for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {
                    ShoppingCart shoppingCart = doc.toObject(ShoppingCart.class);
                    listProductCart.add(shoppingCart);
                }
                cartAdapter.setData(listProductCart);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClearData:

                builder.setTitle(R.string.title_delete_cart);

                builder.setMessage(R.string.delete_cart);

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (final ShoppingCart shoppingCart : listProductCart) {
                            productRepository.getByCodigo(shoppingCart.getCode()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    Product productUpdate = documentSnapshot.toObject(Product.class);
                                    assert productUpdate != null;
                                    productRepository.updateStockByProductCode(productUpdate);
                                    shoppingCarFirestore.deleteProduct(shoppingCart);
                                    cartAdapter.updateData();
                                    btnClearData.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                });

                builder.show();

                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save_cart, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_save:
                builder.setTitle(R.string.compra);

                builder.setMessage(R.string.buy_cart);

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (final ShoppingCart shoppingCart : listProductCart) {
                            shoppingCarFirestore.deleteProduct(shoppingCart);
                            cartAdapter.updateData();
                            btnClearData.setVisibility(View.GONE);
                        }
                    }
                });

                builder.show();
                return true;

            default:

                return super.onOptionsItemSelected(item);
        }
    }
}
