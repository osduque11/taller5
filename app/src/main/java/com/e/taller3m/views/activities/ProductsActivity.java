package com.e.taller3m.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.e.taller3m.R;
import com.e.taller3m.adapters.ProductAdapter;
import com.e.taller3m.database.Product_Repository;
import com.e.taller3m.model.Product;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ProductsActivity extends AppCompatActivity {

    ArrayList<Product> listProducts;

    ProductAdapter productAdapter;
    RecyclerView rvProducts;
    LinearLayoutManager llManager;

    Product_Repository productRepository;
    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        listProducts = new ArrayList<>();
        category = getIntent().getStringExtra("category");

        assert category != null;
        if (!category.isEmpty()) {
            productRepository = new Product_Repository(this);
            initUI();
            getSupportActionBar().setTitle(category);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getProductsByCategory();

    }

    public void getProductsByCategory() {
        productRepository.getProductsByCategory(category).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                listProducts.clear();
                for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                    Product product = document.toObject(Product.class);
                    listProducts.add(product);
                }
                productAdapter.setData(listProducts);
            }
        });
    }

    private void initUI() {

        rvProducts = findViewById(R.id.rvProducts);
        productAdapter = new ProductAdapter(listProducts);
        llManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvProducts.setLayoutManager(llManager);
        rvProducts.setAdapter(productAdapter);

        getProductsByCategory();

        productAdapter.setListener(new ProductAdapter.onItemClickListener() {
            @Override
            public void onClick(int pos) {
                Intent intent = new Intent(ProductsActivity.this, ProductDetailActivity.class);
                intent.putExtra("currentProduct", listProducts.get(pos));
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //flecha para devolver

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
